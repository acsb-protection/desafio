pipeline {
    agent any

    environment { 
        registry = "acsb30/desafio" 
        registryCredential = 'dockerhub_id' 
        dockerImage = '' 
    }

    stages {
        stage('Clone Repository') {
            steps {
                git url: 'https://gitlab.com/acsb-protection/desafio.git'
            }
        }
        stage('Build Docker Image') {
            steps {
                script {
                    dockerImage = docker.build registry + ":develop"
                }
            }
        }
        stage('Send image to Docker Hub') {
            steps {
                script {
                    docker.withRegistry( '', registryCredential ) { 
                    dockerImage.push() 
                    }
                }
            }
        }
    	stage('Deploy') {
		    steps{
                    step([$class: 'AWSCodeDeployPublisher', 
                        applicationName: 'desafio-application',
                        awsAccessKey: "AKIAIMKZIB5KDBYQB4RA",
                        awsSecretKey: "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",
                        credentials: 'awsAccessKey',
                        deploymentGroupAppspec: false,
                        deploymentGroupName: 'service',
                        deploymentMethod: 'deploy',
                        excludes: '',
                        iamRoleArn: '',
                        includes: '**',
                        pollingFreqSec: 15,
                        pollingTimeoutSec: 600,
                        proxyHost: '',
                        proxyPort: 0,
                        region: 'us-east-1',
                        s3bucket: 'aulas3curso', 
                        s3prefix: '', 
                        subdirectory: '',
                        versionFileName: '',
                        waitForCompletion: true])
            }
        }
        stage('Cleaning up') {
            steps {
                sh "docker rmi $registry:develop"
            }
        }
    }
}